//
//  Constants.swift
//  NearWithYou
//
//  Created by Diana on 07.06.16.
//  Copyright © 2016 Diana. All rights reserved.
//

struct Constants {
    
    struct NotificationKeys {
        static let SignedIn = "onSignInCompleted"
    }
    
    struct Segues {
        static let SignInToFp = "SignInToFP"
        static let FpToSignIn = "FPToSignIn"
}
}