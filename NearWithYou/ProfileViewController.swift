//
//  Profile.swift
//  NearWithYou
//
//  Created by Diana on 07.06.16.
//  Copyright © 2016 Diana. All rights reserved.
//
import UIKit

import Firebase

class ProfileViewController: UIViewController {
    

    override func viewDidLoad() {
        _ = FIRDatabase.database().reference()

        super.viewDidLoad()
    }
    override func viewDidAppear(animated: Bool) {
    }
    
    @IBAction func signOUT_button(sender: UIButton) {
        
        
        let firebaseAuth = FIRAuth.auth()
        do {
            try firebaseAuth?.signOut()
            AppState.sharedInstance.signedIn = false
            dismissViewControllerAnimated(true, completion: nil)
        } catch let signOutError as NSError {
            print ("Error signing out: \(signOutError)")
        }
        
        
    }
    
    
    
    }





