//
//  SignInViewController.swift
//  NearWithYou
//
//  Created by Diana on 07.06.16.
//  Copyright © 2016 Diana. All rights reserved.
//

import UIKit
import Firebase

class SignInViewController: UIViewController {
            // Do any additional setup after loading the view, typically from a nib.


    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBOutlet weak var emailField: UITextField!
   
    @IBOutlet weak var passwordField: UITextField!

    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //перед выводом view
    override func viewDidAppear(animated: Bool) {
        if let user = FIRAuth.auth()?.currentUser {
            self.signedIn(user)
        }
    }
    

    //аутенфикация пользователя
    @IBAction func signIN_button(sender: AnyObject) {
        
        let email = emailField.text
        let password = passwordField.text
        
        FIRAuth.auth()?.signInWithEmail(email!, password: password!, completion: { (user, error) in
            if let error = error {
                
                print(error.localizedDescription)
                return

            }
            self.signedIn(user!)
        })
        
    }
    
    func signedIn(user: FIRUser?) {
        MeasurementHelper.sendLoginEvent()
        
        AppState.sharedInstance.signedIn = true
        NSNotificationCenter.defaultCenter().postNotificationName(Constants.NotificationKeys.SignedIn, object: nil, userInfo: nil)
        performSegueWithIdentifier(Constants.Segues.SignInToFp, sender: nil)
    }
    
    //создание аккаунта
    @IBAction func createAccount_button(sender: AnyObject) {
        
        let email = emailField.text
        let password = passwordField.text
        
        FIRAuth.auth()?.createUserWithEmail(email!, password: password!, completion: { (user, error) in
            if let error = error {
                
                print(error.localizedDescription)
                return
                
            }
            self.setDisplayName(user!)
        })
        
        
    }
    
    func setDisplayName(user: FIRUser) {
        let changeRequest = user.profileChangeRequest()
        changeRequest.displayName = user.email!.componentsSeparatedByString("@")[0]
        changeRequest.commitChangesWithCompletion(){ (error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            self.signedIn(FIRAuth.auth()?.currentUser)
        }
    }
    
    

    //Восстановление пароля
    @IBAction func forgot_Button(sender: AnyObject) {
        
        let prompt = UIAlertController.init(title: nil, message: "Email:", preferredStyle: UIAlertControllerStyle.Alert)
        let okAction = UIAlertAction.init(title: "OK", style: UIAlertActionStyle.Default) { (action) in
            let userInput = prompt.textFields![0].text
            if (userInput!.isEmpty) {
                return
            }
            FIRAuth.auth()?.sendPasswordResetWithEmail(userInput!) { (error) in
                if let error = error {
                    print(error.localizedDescription)
                    return
                }
            }
        }
        prompt.addTextFieldWithConfigurationHandler(nil)
        prompt.addAction(okAction)
        presentViewController(prompt, animated: true, completion: nil);
        
        
    }

}
