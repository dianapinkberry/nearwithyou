//
//  CreateAccountViewController.swift
//  NearWithYou
//
//  Created by Diana on 31.05.16.
//  Copyright © 2016 Diana. All rights reserved.
//

import UIKit
import Firebase


class CreateAccountViewController: UIViewController {

    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var passWord: UITextField!
    
    
    
    @IBAction func createAccount(sender: AnyObject) {
        
        self.dismissViewControllerAnimated(true, completion: nil)
        
        if userName.text != nil && passWord.text != nil {
            FIRAuth.auth()?.createUserWithEmail(userName.text!, password: passWord.text!, completion: {(usser, error) in
                if error == nil
                {
                    self.display("Success")
                }
                else
                {
                    self.display("Faild")
                }
        
        
            })
        }
    }


        func display(message: String)
        {
            let alert = UIAlertController(title: "Login State", message: message, preferredStyle: .Alert)
            self.presentViewController(alert,animated: true, completion: nil)
            let okay = UIAlertAction(title: "Okay", style: .Default, handler: nil)
            alert.addAction(okay)
        }
        
        

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */


