//
//  ViewController.swift
//  NearWithYou
//
//  Created by Diana on 31.05.16.
//  Copyright © 2016 Diana. All rights reserved.
//

import UIKit
import Firebase



class LoginViewController: UIViewController {

    
    
    @IBOutlet weak var log_userName: UITextField!
    @IBOutlet weak var log_passWord: UITextField!
    
    @IBOutlet weak var signUP: UIButton!
    @IBOutlet weak var forgotPassword: UIButton!
    
    
    @IBAction func logIN(sender: AnyObject) {
        
        self.dismissViewControllerAnimated(true, completion: nil)
        
        if log_userName.text != nil && log_passWord.text != nil {
            FIRAuth.auth()?.signInWithEmail(log_userName.text!, password: log_passWord.text!, completion: { (usser, error) in
                if error == nil
                {
                    self.display("Succsess")
                }
                    else
                {
                    self.display("Failed")
                }
                
                })
        }
    }
    
            func display(message: String)
            {
                let alert = UIAlertController(title: "Login", message: message, preferredStyle: .Alert)
                self.presentViewController(alert,animated: true, completion: nil)
                let okay = UIAlertAction(title: "Okay", style: .Default, handler: nil)
                alert.addAction(okay)
                
                
            }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
        
    }



